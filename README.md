# Thread Dump Scripts #

These scripts are designed to provide an easier way to generate thread dumps on Windows, and thread dumps combined with CPU data on Linux.

We recommended to install the ﻿[Thready﻿](https://marketplace.atlassian.com/plugins/com.atlassian.ams.shipit.tomcat-filter/server/overview) plugin, if it is supported by your Atlassian application version. This will provide additional details on the Tomcat threads (namely the URI) and will allow for faster troubleshooting.

## Linux ##

This script allows you to:

* Check your Product Home & for JIRA the indexes disk access speed.
* Collects thread dumps, CPU usage (top) and pmap data for the product specified.

### Installation ###

The scripts can either be checked out from the repository or downloaded and run as below.

### Usage ###

To run them:

    wget https://bitbucket.org/atlassianlabs/atlassian-support/raw/master/support-data.sh  # If you don't have a local clone of this repo
    chmod a+x support-data.sh

    ./support-data.sh {product}

The available products are:

* JIRA
* CONFLUENCE
* BITBUCKET
* STASH
* FISHEYE
* BAMBOO

You must run the script as the same user that is running the Atlassian application.

You can refer to the [Disk Access Speed gradings](https://confluence.atlassian.com/display/JIRAKB/Testing+Disk+Access+Speed#TestingDiskAccessSpeed-grade) to verify the results.

Then attach the generated tar.gz to your support ticket.

Notes:

* You can find the sources for the downloaded `support-tools.jar` in https://bitbucket.org/juliasimon/atlassian-support-benchmark/overview
* Alternatively you can check out the repo and execute it from the command line.

## Windows ##

These scripts allow you to collect thread dumps more easily in Windows.  For Windows, only JIRA is currently supported.

### Installation ###

1.  `PsExec` is a part of Microsoft's PsTools suite. It can be downloaded from [the Microsoft website](https://technet.microsoft.com/en-au/sysinternals/bb897553.aspx). These scripts will not run without `PsExec`.  `PsExec` should be placed into the same folder as these batch files. Alternatively, you can edit `setvars.bat` to point to an existing PsExec executable.
1. You have a JDK installed to access the `jstack` utility. You can download one from [Oracle](http://www.oracle.com/technetwork/java/javase/downloads/index.html). Configure `setvars.bat` to point to `jstack.exe` in the JDK's `bin` directory.
1. Download the repository from the [downloads](https://bitbucket.org/cdrummond/atlassian-support/downloads) section and copy the files onto the server running the Atlassian product.
1. After doing so, edit the directories in the `setvars.bat` file to point to the correct directories.
1. Run the scripts with `system.bat`.

Notes:

* When `PsExec` is invoked, it will automatically accept the EULA (due to quirky behaviour with the EULA window; and the fact that it will be called multiple times). You may read the [EULA here](https://technet.microsoft.com/en-au/sysinternals/bb469936).
* Atlassian standalone products ship with a JRE, this does *not* have `jstack` installed.  You will need to have Oracle JDK installed for this to work.  Please see [Installing Java for Confluence](https://confluence.atlassian.com/display/DOC/Installing+Java+for+Confluence) for steps on how to do this.

### Usage ###

First, determine if you're using System Account, or a Dedicated account for your service:

1. Open Services.msc, Right Click on the Service and select "Properties".
1. Choose the "Log On" Tab.

The log on type will determine which batch file to execute:

* If "Local System Account" is ticked, you'll need to use `system.bat`.
* If "This Account" is ticked, you must use `user.bat` - you will also need to know the password for this account.

### Running the Batch File ###
1. Run the selected batch file as an administrator.
1. You'll be prompted for the Process ID (PID) of the process. You can find this with Task Manager. For Atlassian applications, look for `tomcat.exe`. You may need to add the PID tab to do this.
1. Specify the number of thread dumps you require. The recommend default is 5 or 10.
1. Specify the interval between thread dumps in seconds. The recommended default is 5 or 10.
1. If you're using `user.bat` you'll be prompted for the username, followed by the password.
1. The thread dumps will be saved to the current directory - attach them to the support issue in [support.atlassian.com](https://support.atlassian.com)