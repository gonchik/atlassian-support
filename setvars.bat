@ECHO OFF
REM Atlassian Thread Dumps for Windows
REM Author: Dave Norton
REM URL: https://bitbucket.org/dave_norton/windows-thread-dumps

REM This file sets up variables for use when generating thread dumps - just change the paths to suit!

SET JSTACK="C:\Program Files\Java\jdk1.8.0_45\bin\jstack.exe"
SET PSEXEC="PsExec.exe"
